﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		double ticketpreis;
		int anzahltickets;
		boolean wiederholen = true;

		while (wiederholen) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
		}
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("Klirr Klirr Klirr Disch");
		System.out.println(betrag + " " + einheit);
		warte(700);
	}

	public static double fahrkartenbestellungErfassen() {

		double zuZahlenderBetrag = 0;
		int anzahltickets = 0;
		double ticketpreis = 1;
		int fahrkarte;
		boolean wiederholen = true;
		boolean redo = true;
		double sum = 0;
		String[] tickets = { "Einzelfahrschein Berlin AB ", "Einzelfahrschein Berlin BC ",
				"Einzelfahrschein Berlin ABC ", "Kurzstrecke ", "Tageskarte Berlin AB ", "Tageskarte Berlin BC ",
				"Tageskarte Berlin ABC ", "Kleingruppen-Tageskarte Berlin AB ", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC " };
		double[] preis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		Scanner tastatur = new Scanner(System.in);

		while (wiederholen) {
			for (int i = 0; i < tickets.length; i++) {
				System.out.println(tickets[i] + preis[i] + " Euro " + "[" + (i + 1) + "]");
			}
			System.out.println("Bezahlen [" + (preis.length + 1) + "]");
			System.out.println("\n\nWählen Sie ihre Wunschfahrkarte aus:\n");
			fahrkarte = tastatur.nextInt();
			{
				if (fahrkarte > 0 && fahrkarte <= preis.length) {

					do {

						System.out.println("Gebe die Anzahl der Fahrkarten an(Zwischen 1 und 10): ");
						anzahltickets = tastatur.nextInt();
						if (anzahltickets < 1 || anzahltickets > 10) {
							System.out.println("Bitte gebe eine Zahl zwischen 1 und 10 an\n");
							warte(350);
						}
					} while (anzahltickets < 1 || anzahltickets > 10);
					ticketpreis = preis[(fahrkarte - 1)];
					zuZahlenderBetrag = zuZahlenderBetrag + (anzahltickets * ticketpreis);
					System.out.printf("Die aktuellen Kosten belaufen sich auf: %.2f ", zuZahlenderBetrag,
							" Euro\n\n\n\n");
					warte(1000);

				} else if (fahrkarte == (preis.length + 1)) {
					wiederholen = false;
				}
				else {
					System.out.println("Bitte geben Sie ein vorhandendes Ticket an!");
				}
			}
			
			warte(1000);
		}
		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double Zuzahlen) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double rückgabebetrag;
		Scanner tastatur = new Scanner(System.in);
		while (eingezahlterGesamtbetrag < Zuzahlen) {
			System.out.printf("Noch zu zahlen:%.2f Euro\n", (Zuzahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		rückgabebetrag = eingezahlterGesamtbetrag - Zuzahlen;

		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("Die Fahrkarten werden gedruckt!");
		for (int i = 1; i <= 8; i++) {
			System.out.print("=");
			warte(500);
		}

		System.out.println("");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "Euro");
				rückgabebetrag -= 2.0;
				warte(500);
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "Euro");
				rückgabebetrag -= 1.0;
				warte(500);
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "Cent");
				rückgabebetrag -= 0.5;
				warte(500);
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "Cent");
				rückgabebetrag -= 0.2;
				warte(500);
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "Cent");
				rückgabebetrag -= 0.1;
				warte(500);
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "Cent");
				rückgabebetrag -= 0.05;
				warte(500);
			}
		}
	}
}
